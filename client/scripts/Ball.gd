extends RigidBody

onready var club = get_parent().get_node("Club")

func hit() -> void:
	var power = self.club.get_power()
	var direction = self.club.get_direction()
	if power and direction:
		# Minigolf is about putting, so let's disable elevation on the
		# player's shot.
		direction.y = 0
		apply_central_impulse(direction * power)
