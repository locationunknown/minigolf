extends Node

onready var club = get_node("Club")

func _input(event):
	match event.get_class():
		'InputEventMouseButton':
			self.onMouseClick(event)

func onMouseClick(event: InputEventMouseButton) -> void:
	match event.pressed:
		true:
			self.club.charge()
		false:
			self.club.shoot()
