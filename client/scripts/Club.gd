extends Node

onready var ball = get_parent().get_node("Ball")
var _start_time = null
var _end_time = null

func charge() -> void:
	self._start_time = OS.get_ticks_msec()

func shoot() -> void:
	self._end_time = OS.get_ticks_msec()
	self.ball.hit()

func get_power() -> int:
	if self._end_time and self._start_time:
		return (self._end_time - self._start_time) / 10
	else:
		return 0

func get_direction() -> Vector3:
	var viewport = self.get_viewport()
	var mouse_position = viewport.get_mouse_position()
	var camera = viewport.get_camera()
	return camera.project_ray_normal(mouse_position)
