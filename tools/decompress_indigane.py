import re
from io import StringIO

map_format_charmap = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
mapstring = 'CYQB3A47DB3A48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE47DCBQ'
# mapstring = 'BAMM48DEBIMAB3A8DBJMAE24DBIMAB3A8DBJMAEEB3A10DE24DB3A10D3EDCAAE7DE24DE7DCBAED3E10DE24DE10DEEBLMAE9DE24DE9DBKMAEE5DBJAME4DBLAME22DBKAME4DBIAMBAMM5DE6DE9DBJMAE12DBIMAB3A9DBAMM6DE6DE10DE12DB3A10DE6DE6DE10DE12DE10DE6DE6DE10DE12DE10DE6DE6DBLMAE9DE12DE9DBKMAE6DE11DBJAME4DBLAME10DBKAME4DBIAMBAMM11DE12DE9DBJMAEBIMAB3A9DBAMM12DE12DE10DEG10DE12DE12DE10DEE10DE12DE12DE10DEE10DE12DE12DBLMAE9DEE9DBKMAE12DE17DBJAME4DBOAME4DBIAMBAMM17DE18DE10DBAMM18DE18DE10DE18DE18DE10DE18DE18DE10DE18DE18DBLMAE8DBKMAE18DE48D'


def make_2d_list(w, h):
  return [[0 for y in range(h)] for x in range(w)]


def integer_from_charmap(character):
  return map_format_charmap.index(character)


class Tile:
  TYPE_NULL = 0 # maybe?
  TYPE_NORMAL = 1
  TYPE_SPECIAL = 2

  def __init__(
    self,
    # default Tile (1, 0, 0, 0) is grass
    tile_type=TYPE_NORMAL,
    shape_index=0,
    background_element_index=0,
    foreground_element_index=0,
  ):
    self.shape_index = shape_index
    self.tile_type = tile_type
    self.background_element_index = background_element_index
    self.foreground_element_index = foreground_element_index

  def __repr__(self):
    # return f'{self.shape_index}, {self.foreground_element_index}, {self.background_element_index}, {self.tile_type}'
    # return str(int(self.tile_type))
    if int(self.tile_type) == 1:
      return '#'
    else:
      return str(self.shape_index)


def load_map_from_string(mapstring):
  expanded_mapstring = expand_mapstring(mapstring)
  tiles = parse_mapstring(expanded_mapstring)
  return tiles


def expand_mapstring(mapstring):
  # Search and replace for example 3A with AAA and 4B with BBBB
  character_repeat_regex = r'(?P<repeat_count>\d+)(?P<character_to_repeat>\D)'

  def replace_match_with_repeated_characters(match):
    return int(match.group('repeat_count')) * match.group('character_to_repeat')

  return re.sub(
    character_repeat_regex,
    replace_match_with_repeated_characters,
    mapstring,
  )


def parse_mapstring(mapstring):
  mapstring_reader = StringIO(mapstring)
  tiles = make_2d_list(49, 25)

  TILE_TYPES = 'ABC'
  NORMAL_TILE = 'B'
  NULL_OR_SPECIAL = 'AC'

  for y in range(25):
    for x in range(49):
      tile_character = mapstring_reader.read(1)
      if tile_character in TILE_TYPES:
        tile_type = integer_from_charmap(tile_character)
        shape = integer_from_charmap(mapstring_reader.read(1))
        background = integer_from_charmap(mapstring_reader.read(1))
        if tile_character == NORMAL_TILE:
          foreground = integer_from_charmap(mapstring_reader.read(1))
        elif tile_character in NULL_OR_SPECIAL:
          foreground = 0
        tiles[x][y] = Tile(
          tile_type=tile_type,
          shape_index=shape,
          background_element_index=background,
          foreground_element_index=foreground,
        )
      else:
        # Backreferences to previous tiles
        if tile_character == 'D':
          tiles[x][y] = tiles[x - 1][y]
        if tile_character == 'E':
          tiles[x][y] = tiles[x][y - 1]
        if tile_character == 'F':
          tiles[x][y] = tiles[x - 1][y - 1]
        if tile_character == 'G':
          tiles[x][y] = tiles[x - 2][y]
        if tile_character == 'H':
          tiles[x][y] = tiles[x][y - 2]
        if tile_character == 'I':
          tiles[x][y] = tiles[x - 2][y - 2]
  return tiles



print('\n'.join(
  [repr(foo) for foo in load_map_from_string(mapstring)]
))