import re
from enum import Enum
from io import StringIO


def make_2d_list(w, h, default=lambda: 0):
  return [[default() for y in range(h)] for x in range(w)]


class Category(Enum):
  UNKNOWN = -1
  ALL = 0
  BASIC = 1
  TRADITIONAL = 2
  MODERN = 3
  HIO = 4
  SHORT = 5
  LONG = 6


class Tile:
  TYPE_NULL = 0 # maybe?
  TYPE_NORMAL = 1
  TYPE_SPECIAL = 2

  def __init__(
    self,
    # default Tile (1, 0, 0, 0) is grass
    tile_type=TYPE_NORMAL,
    shape_index=0,
    background_element_index=0,
    foreground_element_index=0,
  ):
    self.shape_index = shape_index
    self.tile_type = tile_type
    self.background_element_index = background_element_index
    self.foreground_element_index = foreground_element_index


class Advert:
  SIZE_SMALL = 0
  SIZE_MEDIUM = 1
  SIZE_LARGE = 2
  SIZE_FULL = 3

  def __init__(
    self,
    size=SIZE_SMALL,
    position_x=0,
    position_y=0,
  ):
    self.size = size
    self.position_x = position_x
    self.position_y = position_y


class Track:
  FILE_FORMAT_CHARMAP = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'

  def __init__(self):
    # Track details
    self.version: int = 0
    self.author: str = ''
    self.name: str = ''
    self.categories: list[Category] = []

    # Track statistics
    self.completed_count: int = 0
    self.total_stroke_count: int = 0
    self.best_score: int = 0
    self.best_score_count: int = 0
    self.first_record_name: str = ''
    self.first_record_timestamp_microseconds: int = 0
    self.latest_record_name: str = ''
    self.latest_record_timestamp_microseconds: int = 0
    self.rating_counts: tuple[int] = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

    # Track settings
    self.are_mines_visible: bool = False
    self.are_magnets_visible: bool = True
    self.are_teleporters_colored: bool = True
    self.fake_walls_have_shadows: bool = True

    # Track content
    self.width: int = 49
    self.height: int = 25
    self.tiles: list[Tile] = make_2d_list(self.width, self.height, default=Tile)
    self.adverts: list[Advert] = []

  def load_from_file(self, filename):
    with open(filename, 'r') as track_file:
      for line in track_file.read().splitlines():
        match line.split(' ', 1):
          case 'V', version:
            self.version = int(version)
          case 'A', author:
            self.author = author
          case 'N', name:
            self.title = name
          case 'T', track_data:
            self.set_track_data_from_string(track_data)
          case 'C', categories:
            self.categories = list(map(lambda category: Category(int(category)), categories.split(',')))
          case 'I', statistics:
            self.completed_count, self.total_stroke_count, self.best_score, self.best_score_count = statistics.split(',')
          case 'R', ratings:
            self.rating_counts = list(map(int, ratings.split(',')))
          case 'B', best_record:
            self.first_record_name, first_record_timestamp = best_record.split(',')
            self.first_record_timestamp_microseconds = int(first_record_timestamp)
          case 'L', latest_record:
            self.latest_record_name, latest_record_timestamp = latest_record.split(',')
            self.latest_record_timestamp_microseconds = int(latest_record_timestamp)
          case 'S', settings:
            self.set_settings_from_string(settings)
          case _:
            raise Exception(f'Unrecognized line format in {filename}: {line}')

  @classmethod
  def convert_file_format_characters_to_integers(cls, character_to_convert: str) -> int:
    try:
      return cls.FILE_FORMAT_CHARMAP.index(character_to_convert)
    except:
      print(character_to_convert)
      raise

  def set_track_data_from_string(self, track_data):
    try:
      tiles_string, adverts_string = track_data.split(',Ads:')
    except ValueError:
      tiles_string, adverts_string = track_data, ''
    self.set_tiles_from_string(tiles_string)
    self.set_adverts_from_string(adverts_string)

  @staticmethod
  def decompress_run_length_encoding(encoded_string):
    # Search and replace for example 3A with AAA and 4B with BBBB
    character_repeat_regex = r'(?P<repeat_count>\d+)(?P<character_to_repeat>\D)'

    def replace_match_with_repeated_characters(match):
      return int(match.group('repeat_count')) * match.group('character_to_repeat')

    return re.sub(
      character_repeat_regex,
      replace_match_with_repeated_characters,
      encoded_string,
    )

  def set_tiles_from_string(self, tiles_string):
    tiles_string = self.decompress_run_length_encoding(tiles_string)
    tiles_string_reader = StringIO(tiles_string)
    tiles = self.tiles

    TILE_TYPES = 'ABC'
    NORMAL_TILE = 'B'
    NULL_OR_SPECIAL = 'AC'
    NO_FOREGROUND = 'A'

    for y in range(self.height):
      for x in range(self.width):
        tile_character = tiles_string_reader.read(1)
        if tile_character in TILE_TYPES:
          tile_type = tile_character
          shape = tiles_string_reader.read(1)
          background = tiles_string_reader.read(1)
          if tile_character == NORMAL_TILE:
            foreground = tiles_string_reader.read(1)
          elif tile_character in NULL_OR_SPECIAL:
            foreground = NO_FOREGROUND
          tile_type, shape, background, foreground = map(self.convert_file_format_characters_to_integers, [tile_type, shape, background, foreground])
          tiles[x][y] = Tile(
            tile_type=tile_type,
            shape_index=shape,
            background_element_index=background,
            foreground_element_index=foreground,
          )
        else:
          # Backreferences to previous tiles
          if tile_character == 'D':
            tiles[x][y] = tiles[x - 1][y]
          if tile_character == 'E':
            tiles[x][y] = tiles[x][y - 1]
          if tile_character == 'F':
            tiles[x][y] = tiles[x - 1][y - 1]
          if tile_character == 'G':
            tiles[x][y] = tiles[x - 2][y]
          if tile_character == 'H':
            tiles[x][y] = tiles[x][y - 2]
          if tile_character == 'I':
            tiles[x][y] = tiles[x - 2][y - 2]

  def set_adverts_from_string(self, adverts_string):
    self.adverts = []
    # Adverts are in the format A0512, where A is size (in map charmap format),
    # 05 is x coordinate and 12 is y coordinate (in tiles)
    advert_format_regex = r'(?P<advert_size>[A-D])(?P<position_x>\d\d)(?P<position_y>\d\d)'
    for match in re.finditer(advert_format_regex, adverts_string):
      self.adverts.append(Advert(
        size=self.convert_file_format_characters_to_integers(match.group('advert_size')),
        position_x=int(match.group('position_x')),
        position_y=int(match.group('position_y')),
      ))

  def set_settings_from_string(self, settings_string):
    # Settings string is in format fttt14 where t and f are true/false
    # The numbers are currently of unknown use
    self.are_mines_visible = settings_string[0] == 't'
    self.are_magnets_visible = settings_string[1] == 't'
    self.are_teleporters_colored = settings_string[2] == 't'
    self.fake_walls_have_shadows = settings_string[3] == 't'


# track = Track()
# track.load_from_file('1shot.track')

# breakpoint()

# import os
# for filename in os.listdir('tracks'):
#   track = Track()
#   track.load_from_file(f'tracks/{filename}')
#   print({key: value for (key, value) in track.__dict__.items() if key != 'tiles'})
#   print(track.tiles[0][0].__dict__)

