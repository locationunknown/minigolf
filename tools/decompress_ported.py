map_chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
mapstring = 'CYQB3A47DB3A48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE48DE47DCBQ'
# mapstring = 'BAMM48DEBIMAB3A8DBJMAE24DBIMAB3A8DBJMAEEB3A10DE24DB3A10D3EDCAAE7DE24DE7DCBAED3E10DE24DE10DEEBLMAE9DE24DE9DBKMAEE5DBJAME4DBLAME22DBKAME4DBIAMBAMM5DE6DE9DBJMAE12DBIMAB3A9DBAMM6DE6DE10DE12DB3A10DE6DE6DE10DE12DE10DE6DE6DE10DE12DE10DE6DE6DBLMAE9DE12DE9DBKMAE6DE11DBJAME4DBLAME10DBKAME4DBIAMBAMM11DE12DE9DBJMAEBIMAB3A9DBAMM12DE12DE10DEG10DE12DE12DE10DEE10DE12DE12DE10DEE10DE12DE12DBLMAE9DEE9DBKMAE12DE17DBJAME4DBOAME4DBIAMBAMM17DE18DE10DBAMM18DE18DE10DE18DE18DE10DE18DE18DE10DE18DE18DBLMAE8DBKMAE18DE48D'

def get_2d_list(w, h):
    return [[0 for y in range(h)] for x in range(w)]

class Tile:
    def __init__(self, shape_index=0, background_element_index=0, foreground_element_index=0, tile_type=1):  # Default tile is grass
        self.shape_index = shape_index
        self.background_element_index = background_element_index
        self.foreground_element_index = foreground_element_index
        self.tile_type = tile_type  # is_special is bad name, can be of values 1 = normal or 2 = special

    def __repr__(self):
        # return f'{self.shape_index}, {self.background_element_index}, {self.foreground_element_index}, {self.tile_type}'
        # return str(int(self.tile_type))
        if int(self.tile_type) == 1:
            return '#'
        else:
            return str(self.shape_index)

def convert_codes_to_tiles(tiles):
    result = get_2d_list(49, 25)

    for y in range(0, 25):
        for x in range(0, 49):
            tilecode = tiles[x][y]
            tile_type = int(tilecode / (256 * 256 * 256))  # 0x01000000
            shape_index = int(tilecode / (256 * 256) % 256)
            foreground_element_index = int(tilecode / 256 % 256)
            background_element_index = int(tilecode % 256)
            result[x][y] = Tile(shape_index, background_element_index, foreground_element_index, tile_type)
    return result

def parse_map(mapdata):
    cursor = 0
    tiles = get_2d_list(49, 25)

    for y in range(0, 25):
        for x in range(0, 49):
            map_index = map_chars.index(mapdata[cursor])
            if map_index <= 2:  #if input = A, B or C
                if map_index == 1:
                    map_index_one_ahead = map_chars.index(mapdata[cursor + 1])
                    map_index_two_ahead = map_chars.index(mapdata[cursor + 2])
                    map_index_three_ahead = map_chars.index(mapdata[cursor + 3])
                    cursor += 4
                else:
                    map_index_one_ahead = map_chars.index(mapdata[cursor + 1])
                    map_index_two_ahead = map_chars.index(mapdata[cursor + 2])
                    map_index_three_ahead = 0
                    cursor += 3
                tiles[x][y] = (
                    256 * 256 * 256 * map_index +
                    256 * 256 * map_index_one_ahead +
                    256 * map_index_two_ahead +
                    map_index_three_ahead
                )
            else:
                if map_index == 3:  # if input = D
                    tiles[x][y] = tiles[x - 1][y]
                if map_index == 4:  # if input = E
                    tiles[x][y] = tiles[x][y - 1]
                if map_index == 5:  # if input = F
                    tiles[x][y] = tiles[x - 1][y - 1]
                if map_index == 6:  # if input = G
                    tiles[x][y] = tiles[x - 2][y]
                if map_index == 7:  # if input = H
                    tiles[x][y] = tiles[x][y - 2]
                if map_index == 8:  # if input = I
                    tiles[x][y] = tiles[x - 2][y - 2]
                cursor += 1
    return tiles


def load_map_tiles(mapstring):
    expanded_map_data = expand_map(mapstring)
    print(mapstring)
    print(expanded_map_data)
    tiles = parse_map(expanded_map_data)
    tiles = convert_codes_to_tiles(tiles)
    return tiles

def get_int_from_mapstring(mapstring, cursor):
    result = ''
    while True:
        character = mapstring[cursor]
        if not character.isnumeric():
            if result:
                return int(result)
            else:
                return 1
        result += character
        cursor += 1

def expand_map(mapstring):
    length = len(mapstring)
    cursor = 0
    result = ''

    while cursor < length:
        how_many = get_int_from_mapstring(mapstring, cursor)
        if how_many >= 2:
            cursor += 1
        if how_many >= 10:
            cursor += 1
        if how_many >= 100:
            cursor += 1
        if how_many >= 1000:
            cursor += 1
        character = mapstring[cursor]
        result += character * how_many
        cursor += 1
    return result

print('\n'.join(
    [repr(foo) for foo in load_map_tiles(mapstring)]
))
